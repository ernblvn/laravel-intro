@extends('master')

@section('title')
    Detail Cast {{$cast->nama}}
@endsection

@section('content')
<h1>{{$cast->nama}}, {{$cast->umur}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Back</a>
@endsection